defmodule Q2Test do
  use ExUnit.Case
  doctest ListaMapa
  doctest ListaAdyacencia

  test "Pregunta 1: Creating and firing ListMapa and ListaAdyacencia" do
    assert ListaMapa.initMap() |>  ListaMapa.fire(["p0"],"A")  == ["p1","p2"]
    assert ListaMapa.initMap() |>  ListaMapa.fire(["p1","p4"],"E")  == ["p1","p4"]
    assert ListaMapa.initMap() |>  ListaMapa.fire(["p1","p4"],"B")  == ["p3","p4"]
    assert ListaAdyacencia.initLista() |>  ListaAdyacencia.fire(["p0"],"B")  == ["p0"]
    assert ListaAdyacencia.initLista() |>  ListaAdyacencia.fire(["p1","p2"],"D")  == ["p3","p4"]
    assert ListaAdyacencia.initLista() |>  ListaAdyacencia.fire(["p3","p4"],"E")  == ["p5"]
  end

  test "Pregunta 2: Using enablement in ListMapa and ListaAdyacencia" do
    assert ListaMapa.initMap() |>  ListaMapa.enablement(["p0"]) == ["A"]
    assert ListaMapa.initMap() |>  ListaMapa.enablement(["p1","p2"]) == ["B","D","C"]
    assert ListaAdyacencia.initLista() |>  ListaAdyacencia.enablement(["p1","p3"]) == ["B"]
    assert ListaAdyacencia.initLista() |>  ListaAdyacencia.enablement(["p2","p4"]) == ["C"]
  end

  test "Pregunta 3: Using replaying in ListMapa and ListaAdyacencia" do
    assert ListaMapa.initMap() |>  ListaMapa.replaying(["p0"],"log1.txt") == [2,8]
    assert ListaMapa.initMap2() |>  ListaMapa.replaying(["p0"],"log1.txt") == [6,4]
    assert ListaAdyacencia.initLista() |>  ListaAdyacencia.replaying(["p0"],"log1.txt") == [2,8]
    assert ListaAdyacencia.initLista2() |>  ListaAdyacencia.replaying(["p0"],"log1.txt") == [6,4]
  end
end
