
defmodule ListaMapa do

  @p0 %{:postset => ["A"], :preset => nil, :label => "p0"}

  @a %{:postset =>  ["p1","p2"], :preset => ["p0"], :label => "A"}

  @p1 %{:postset => ["B","D"], :preset => ["A"], :label => "p1"}
  @p2 %{:postset => ["D","C"], :preset => ["A"], :label => "p2"}

  @b %{:postset => ["p3"], :preset => ["p1"], :label => "B"}
  @d %{:postset => ["p3", "p4"], :preset => ["p1","p2"], :label => "D"}
  @c %{:postset => ["p4"], :preset => ["p2"], :label => "C"}

  @p3 %{:postset => ["E"], :preset => ["B","D"], :label => "p3"}
  @p4 %{:postset => ["E"], :preset => ["C","D"], :label => "p4"}

  @e %{:postset => ["p5"], :preset => ["p3","p4"], :label => "E"}

  @p5 %{:postset => nil, :preset => ["E"], :label => "p5"}

  @p1v2 %{:postset => ["B"], :preset => ["A"], :label => "p1"}
  @p2v2 %{:postset => ["C"], :preset => ["A"], :label => "p2"}
  @dv2 %{:postset => ["p2"], :preset => ["p4"], :label => "D"}
  @p3v2 %{:postset => ["E"], :preset => ["B"], :label => "p3"}
  @p4v2 %{:postset => ["D","E"], :preset => ["C"], :label => "p4"}

  @dv3 %{:postset => ["p3"], :preset => ["p1","p2"], :label => "D"}
  @p4v3 %{:postset => ["E"], :preset => ["C"], :label => "p4"}

  #Pregunta 1
  @red [@p0,@a,@p1,@p2,@b,@d,@c,@p3,@p4,@e,@p5]
  #Pregunta 3
  @red2 [@p0,@a,@p1v2,@p2v2,@b,@dv2,@c,@p3v2,@p4v2,@e,@p5]
  #Pregunta 4
  @red3 [@p0,@a,@p1,@p2,@b,@dv3,@c,@p3,@p4v3,@e,@p5]

  @marcadoInicial [ "p0" ]

  #Función para obtener la estructura básica de una lista de mapas
  def initMap() do
    @red
  end

  #Función para obtener la estructura de una lista de mapas representada en la pregunta 3
  def initMap2() do
   @red2
  end

  #Función para obtener la estructura de una lista de mapas representada en la pregunta 4
  def initMap3() do
   @red3
  end

  #Función para obtener el marcado inicial p0
  def initMarcado() do
    @marcadoInicial
  end

  #Pregunta 1: Función que recibe una estructura de una red de petri,
  #un marcado, y la transicion que se desea hacer. Devuelve el marcado
  #siguiente si la transicion se puede realizar, o el marcado ingresado si no.
  def fire(tree, currentMarcado, transicion) do

    [h|t] = tree
    %{:label => sim} = h

    if(sim == transicion) do
      %{:preset => listPreset} = h

      if listPreset -- currentMarcado == [] do
        updatedMarcado = currentMarcado -- listPreset
        %{:postset => nextMarcado} = h
        Enum.sort(updatedMarcado ++ nextMarcado)
      else
        currentMarcado
      end
    else
      fire(t, currentMarcado, transicion)
    end
  end

  #Pregunta 2: Función que recibe una lista, y un elemento (marcado).
  #Hace uso de las llaves de la lista de mapas
  #Devuelve una lista con las transiciones posibles que se pueden hacer de acuerdo
  #al marcado dado.
  def enablement([],_), do: []
  def enablement(red, marcado) do
    [h|t] = red
    %{:preset => preset} = h
    %{:label => label} = h

    if preset != nil do
      if preset -- marcado == [] do
        List.flatten([label] ++ [enablement(t, marcado)]) -- [nil]
      else
       List.flatten([]  ++ [enablement(t, marcado)]) -- [nil]
      end
    else
       List.flatten([]  ++ [enablement(t, marcado)]) -- [nil]
    end
  end

  #Funcion para leer un archivo textto que contiene caracteres separados por comas y
  #saltos de linea. Devuelve una lista de listas que contienen caracteres String.
  def breakdown(filename) do
    File.read!(filename) |> String.split |> Enum.map(fn line -> String.split(line) |> hd() |> String.split(",") end)
  end

  #Funcion que recibe la lista de mapas, un marcado, y una lista de transiciones
  #Devuelve falso si se encuentra alguna transicion que no se pueda realizar, o
  # verdadero si se llega al final de la lista de transiciones.
  def multiFire(_red, _marcado, []), do: true
  def multiFire(red, marcado, [h|t]) do

    result = fire(red, marcado, h)

    if result == marcado do
      false
    else
      multiFire(red, result, t)
    end
  end

  #Funcion que recibe la lista de mapas, un marcado, y una lista de listas de transiciones
  #Devuelve el numero de veces que una lista de transiciones se pudo ejecutar exitosamente
  def findPositives(_red, _marcado, []), do: 0
  def findPositives(red, marcado, [h|t]) do
    if multiFire(red, marcado, h) == true do
      1 + findPositives(red, marcado, t)
    else
      0 + findPositives(red, marcado, t)
    end
  end

  #Funcion que recibe la lista de mapas, un marcado, y una lista de listas de transiciones
  #Devuelve el numero de veces que una lista de transiciones no se pudo ejecutar exitosamente
  def findNegatives(_red, _marcado, []), do: 0
  def findNegatives(red, marcado, [h|t]) do
    if multiFire(red, marcado, h) == false do
      1 + findNegatives(red, marcado, t)
    else
      0 + findNegatives(red, marcado, t)
    end
  end

  #Pregunta 3: Funcion que recibe la lista de mapas, marcado inicial, y el nombre de un archivo de texto
  #Devuelve el numero de veces que una lista de transiciones se pudo ejecutar exitosamente
  def replaying(red, marcado, file) do
    [h|t] = breakdown(file)
    if multiFire(red, marcado, h) == true do
      [1 + findPositives(red, marcado, t),  0 + findNegatives(red, marcado, t)]
    else
      [0 + findPositives(red, marcado, t),  1 + findNegatives(red, marcado, t)]
    end
  end

  #Pregunta 4: Funcion que recibe la lista de adyacencia, y el marcado inicial de la red de petri,
  # y llama a un acumulador vacio para el override de la funcion. Devuelve una estructura de listas
  # con marcados, transiciones, y los marcados a los que llevan dichas transiciones.
  def reachability_graph(red, marcado) do
      reachability_graph(red, marcado,[])
  end
  def reachability_graph(red, marcado, result) do

      processes = enablement(red,marcado)

      Enum.reduce(processes, result, fn (trans, result) ->
        next = fire(red, marcado, trans)
        if !Enum.member?(result,[marcado,trans,next]) do
             reachability_graph(red,next,result ++ [[marcado, trans, next]])
        else
           reachability_graph(red,next,result)
        end
      end)
    end

end

defmodule ListaAdyacencia do
  #Pregunta 1
  @list [
   ["p0",["A"]],
   ["A",["p1","p2"]],
   ["p1",["B","D"]],
   ["p2",["C","D"]],
   ["B",["p3"]],
   ["C",["p4"]],
   ["D",["p3","p4"]],
   ["p3",["E"]],
   ["p4",["E"]],
   ["E",["p5"]],
   ["p5", []]
  ]
  #Pregunta 3
  @list2 [
   ["p0",["A"]],
   ["A",["p1","p2"]],
   ["p1",["B"]],
   ["p2",["C"]],
   ["B",["p3"]],
   ["C",["p4"]],
   ["D",["p2"]],
   ["p3",["E"]],
   ["p4",["D","E"]],
   ["E",["p5"]],
   ["p5", []]
  ]
  #Pregunta 4
  @list3 [
   ["p0",["A"]],
   ["A",["p1","p2"]],
   ["p1",["B","D"]],
   ["p2",["C","D"]],
   ["B",["p3"]],
   ["C",["p4"]],
   ["D",["p3"]],
   ["p3",["E"]],
   ["p4",["E"]],
   ["E",["p5"]],
   ["p5", []]
  ]

  @marcadoInicial [ "p0" ]

  #Función que regresa la lista de adyacencia básica
  def initLista do
    @list
  end

  #Función que regresa la lista de adyacencia representada en la pregunta 3
  def initLista2 do
    @list2
  end

  #Función que regresa la lista de adyacencia representada en la pregunta 4
  def initLista3 do
  @list3
  end

  #Función para obtener el marcado inicial p0
  def initMarcado() do
    @marcadoInicial
  end

  #Función que recibe una lista y un elemento. Devuelve true si el
  #elemento se encuentra dentro de la lista, o false si no se encuentra.
  def cicle([], _item), do: false
  def cicle([h|_t], item) when h == item, do: true
  def cicle([h|t], item) when h != item, do: cicle(t, item)

  #Función que recibe una lista y un elemento. Devuelve una lista de strings,
  #los cuales forman el preset del elemento (transicion) otorgado.
  def getPreset(list, _) when list == [], do: []
  def getPreset(list, transicion) do
    [h|t] = list
    next = List.flatten(tl(h))

    if cicle(next, transicion) == true do
        [hd(h)] ++ getPreset(t, transicion)
    else
      getPreset(t, transicion)
    end
  end

  #Función que recibe una lista y un elemento. Devuelve una lista de strings,
  #los cuales forman el postset del elemento (transicion) otorgado.
  def getPostset([],_), do: []
  def getPostset(list, transicion) do
    [h|t] = list
    if hd(h) == transicion do
      List.flatten(tl(h))
    else
    getPostset(t,transicion)
    end
  end

  #Pregunta 1: Función que recibe una estructura de una red de petri,
  #un marcado, y la transicion que se desea hacer. Devuelve el marcado
  #siguiente si la transicion se puede realizar, o el marcado ingresado si no.
  def fire(list, currentMarcado, transicion) do
    preset = getPreset(list,transicion)
    postset = getPostset(list,transicion)

    if preset -- currentMarcado == [] do
      updatedMarcado = currentMarcado -- preset
      Enum.sort(postset ++ updatedMarcado)
    else
      currentMarcado
    end
  end

  #Pregunta 2: Función que recibe una lista, la cual se introduce dos veces en un override de la funcion,
  #y un elemento (marcado). Devuelve una lista con las transiciones posibles que se pueden hacer de acuerdo
  #al marcado dado.
  def enablement([], _marcado), do: []
  def enablement(list, marcado) do
    enablement(list,list,marcado)
  end
  def enablement([], _original, _marcado), do: []
  def enablement(current, original, marcado) do
    [h|t] = current
    [label|_pt] = h

    preset = getPreset(original, label)

    if preset != [] do
      if preset -- marcado == [] do
        [label] ++ enablement(t, original, marcado)
      else
        [] ++ enablement(t, original, marcado)
      end
    else
      [] ++ enablement(t, original, marcado)
    end
  end

  #Funcion para leer un archivo textto que contiene caracteres separados por comas y
  #saltos de linea. Devuelve una lista de listas que contienen caracteres String.
  def breakdown(filename) do
  File.read!(filename) |> String.split |> Enum.map(fn line -> String.split(line) |> hd() |> String.split(",") end)
  end

  #Funcion que recibe la lista de mapas, un marcado, y una lista de transiciones
  #Devuelve falso si se encuentra alguna transicion que no se pueda realizar, o
  # verdadero si se llega al final de la lista de transiciones.
  def multiFire(_red, _marcado, []), do: true
  def multiFire(red, marcado, [h|t]) do

    result = fire(red, marcado, h)

    if result == marcado do
      false
    else
      multiFire(red, result, t)
    end
  end

  #Funcion que recibe la lista de mapas, un marcado, y una lista de listas de transiciones
  #Devuelve el numero de veces que una lista de transiciones se pudo ejecutar exitosamente
  def findPositives(_red, _marcado, []), do: 0
  def findPositives(red, marcado, [h|t]) do
    if multiFire(red, marcado, h) == true do
      1 + findPositives(red, marcado, t)
    else
      0 + findPositives(red, marcado, t)
    end
  end

  #Funcion que recibe la lista de mapas, un marcado, y una lista de listas de transiciones
  #Devuelve el numero de veces que una lista de transiciones no se pudo ejecutar exitosamente
  def findNegatives(_red, _marcado, []), do: 0
  def findNegatives(red, marcado, [h|t]) do
    if multiFire(red, marcado, h) == false do
      1 + findNegatives(red, marcado, t)
    else
      0 + findNegatives(red, marcado, t)
    end
  end

  #Pregunta 3: Funcion que recibe la lista de adyacencia, marcado inicial, y el nombre de un archivo de texto
  #Devuelve el numero de veces que una lista de transiciones se pudo ejecutar exitosamente
  def replaying(red, marcado, file) do
    [h|t] = breakdown(file)
    if multiFire(red, marcado, h) == true do
      [1 + findPositives(red, marcado, t),  0 + findNegatives(red, marcado, t)]
    else
      [0 + findPositives(red, marcado, t),  1 + findNegatives(red, marcado, t)]
    end
  end

  #Pregunta 4: Funcion que recibe la lista de adyacencia, y el marcado inicial de la red de petri,
  # y llama a un acumulador vacio para el override de la funcion. Devuelve una estructura de listas
  # con marcados, transiciones, y los marcados a los que llevan dichas transiciones.
  def reachability_graph(red, marcado) do
    reachability_graph(red,marcado,[])
  end
  def reachability_graph(red, marcado, result) do

    processes = enablement(red,marcado)

    Enum.reduce(processes, result, fn (trans, result) ->
      next = fire(red, marcado, trans)
      if !cicle(result,[marcado,trans,next]) do
          reachability_graph(red,next,result ++ [[marcado, trans, next]])
      else
        reachability_graph(red,next,result)
      end
    end)
  end
end
