Nombre: Sahid Emmmanuel Rosas Maas
Matricula: A01734211

Reflexión:

Identificar las diferencias entre las funciones fire() codificadas es importante para reconocer la facilidad
con la que una estructura de datos puede ser más flexible, o más fácil de usar, que otra. La gran diferencia
entre estas dos funciones es el uso de mapas y de funciones auxiliares, ya que con la estructura que usa una
lista de mapas se toma la ventaja de las llaves que estas ofrecen para conocer el valor del preset que tienen de forma
natural, a comparación de las listas de adyacencia cuya única forma de conocer el preset de cada elemento es
a través de una función auxiliar. Y es por medio de estas funciones auxiliares que se nota otra gran diferencia, el cual es el uso de recursión. Mientras que la función fire() de ListMapa usa recursión, la de ListaAdyacencia solo llama a
sus funciones auxiliares para realizar una rápida comparación.

Con lo anterior en mente, podemos evaluar cada estructura y comparar sus fortalezas y sus debilidades. En especial,
la lista de mapas es muy robusta, pero fácil de manejar, ya que con cada nodo se pueden obtener los postsets y los
presets, y así realizar todas las operaciones necesarias con menos problemas. En cambio, la lista de adyacencia solo
usa listas anidadas, por lo cual solo se conocen los postset de cada nodo y requiere de otros métodos más complicados
para acceder a sus presets y realizar las operaciones necesarias. De esta forma concluyo que, una lista de mapas es una
estructura muy ventajosa, ya que hace uso de los mapas y sus métodos tan eficientes y permite obtener ciertos datos
de una forma muy rápida, mientras que una lista de adyacencia es más primitiva y ciertamente flexible, pero se complica
mucho en cuanto a la obtención de datos y la elaboración de funciones complejas.
